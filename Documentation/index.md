
# Module 114 Documentation

Dies ist meine Documentation für das Modul 114. 


## Inhalte

| Name | Link | Beschreibung | 
|---|---|---|
| Stellenwertsystem | [[Stellenwertsystem]] | Die Aufgaben zu _Stellenwertsystem_ |
| Binäre Codierung | [[Binaere Codierung]] | Die Aufgaben zu _Binäre Codierung_ |


## Changelog

| Datum | Tätigkeit |
|---|---|
| 01/11/2023 | Erstellen der mkdocs Webseite und iniziieren des Repos |
| 08/11/2023 | Stellenwertsystem & Binäre Codierung hinzufügen |

