# Stellenwertsystem

## Kurze Beschreibung

Das Zahlensystem das im Alltag am meisten verwendet wird, ist das Dezimalsystem. Dieses hat die Basis 10. Die _Basis des Zahlensystem_ legt fest, wie viele verschiedene Nummern durch eine Stelle dargestellt werden kann. Im Fall vom Dezimalsystem sind das 10. Also `0..9`. Es gibt aber auch noch andere Zahlensysteme, die die am meisten verwendet werden ist das Binärsystem (Basis 2, also `0/1`), das Oktalsystem (Basis 8, also `0..7`) und das Hexadezimalsystem (Basis 16, also `0..f`). 


## Verwendung des Binärsystems

Das Binärsystems wird heute im Zusammenhang mit Computern verwendet. Der Grund dafür ist, das Computer in Bit und Byte "rechnen", bzw. kommunizieren. Ein Byte ist durch 8 Bit dargestellt, die jeweils 0 oder 1 sind. Da die Computer das Binärsystems dazu verwenden um zu arbeiten, macht es sinn, das ein Informatiker sich mit diesem auskennt.


## Aktuelle Verwendung des Oktalsystems

Das Oktalsystem wird in der heutigen Zeit **in erster Linie in der Digitaltechnik** eingesetzt. Neben der Nutzung zur Darstellung der klassischen __Unix Dateisystem Zugriffsrechte__ findet es aber auch noch in der __Flugsicherung__ ein Anwendungsgebiet.


## Vorkenntnisse über das Hexadezimalsystem

Das Hexadezimalsystem wird heute oft im Zusammenhang mit Computern verwendet. Der Grund dafür ist, das Computer in Bit und Byte "rechnen", bzw. kommunizieren. Ein Byte ist durch 8 Bit dargestellt, die jeweils 0 oder 1 sind. Nun wird das Hexadezimalsystem dazu verwendet, um ein Byte einfacher darzustellen, den 2 Stellen im Hexadezimalsystem können genau den Wert eines Bytes beschreiben. Deshalb wird das Hexadezimalsystem z.B. mit MAC- oder IPv6-Adressen verwendet.


## Umwandlung von Zahlensystemen

1. Dezimal -> Binär:
    - Schritt 1: Teile die Dezimalzahl durch 2 und notiere den Rest.
    - Schritt 2: Wiederhole Schritt 1 mit dem Ergebnis der Division, bis das Ergebnis 0 ist.
    - Schritt 3: Lies die Reste von unten nach oben, um die Binärzahl zu erhalten.

2. Dezimal -> Oktal:
    - Schritt 1: Teile die Dezimalzahl durch 8 und notiere den Rest.
    - Schritt 2: Wiederhole Schritt 1 mit dem Ergebnis der Division, bis das Ergebnis 0 ist.
    - Schritt 3: Lies die Reste von unten nach oben, um die Oktalzahl zu erhalten.

3. Dezimal -> Hexadezimal:
    - Schritt 1: Teile die Dezimalzahl durch 16 und notiere den Rest.
    - Schritt 2: Wiederhole Schritt 1 mit dem Ergebnis der Division, bis das Ergebnis 0 ist.
    - Schritt 3: Verwandle die Reste von Zahlen größer als 9 in die entsprechenden Buchstaben A bis F, um die Hexadezimalzahl zu erhalten.

4. Binär -> Oktal:
    - Schritt 1: Gruppiere die Binärziffern in Dreiergruppen von rechts nach links.
    - Schritt 2: Konvertiere jede Dreiergruppe in die entsprechende Oktalziffer.

5. Binär -> Dezimal:
    - Schritt 1: Multipliziere jede Binärziffer mit 2 hoch ihrer Position und addiere die Ergebnisse.

6. Binär -> Hexadezimal:
    - Schritt 1: Gruppiere die Binärziffern in Vierergruppen von rechts nach links.
    - Schritt 2: Konvertiere jede Vierergruppe in die entsprechende Hexadezimalziffer.

7. Oktal -> Binär:
    - Schritt 1: Konvertiere jede Oktalziffer in ihren entsprechenden Binärcode.

8. Oktal -> Dezimal:
    - Schritt 1: Multipliziere jede Oktalziffer mit 8 hoch ihrer Position und addiere die Ergebnisse.

9. Oktal -> Hexadezimal:
    - Schritt 1: Konvertiere zuerst die Oktalzahl in eine Dezimalzahl und dann die Dezimalzahl in eine Hexadezimalzahl.

10. Hexadezimal -> Binär:
    - Schritt 1: Konvertiere jede Hexadezimalziffer in ihren entsprechenden Binärcode.

11. Hexadezimal -> Oktal:
    - Schritt 1: Konvertiere zuerst die Hexadezimalzahl in eine Dezimalzahl und dann die Dezimalzahl in eine Oktalzahl.

12. Hexadezimal -> Dezimal:
    - Schritt 1: Multipliziere jede Hexadezimalziffer mit 16 hoch ihrer Position und addiere die Ergebnisse.


### Beispiele

1. Dezimal -> Binär:
    - Beispiel: Dezimalzahl 13
        - 13 / 2 = 6 Rest 1
        - 6 / 2 = 3 Rest 0
        - 3 / 2 = 1 Rest 1
        - 1 / 2 = 0 Rest 1
        - Binärzahl: 1101

2. Dezimal -> Oktal:
    - Beispiel: Dezimalzahl 79
        - 79 / 8 = 9 Rest 7
        - 9 / 8 = 1 Rest 1
        - 1 / 8 = 0 Rest 1
        - Oktalzahl: 117

3. Dezimal -> Hexadezimal:
    - Beispiel: Dezimalzahl 255
        - 255 / 16 = 15 Rest 15 (F)
        - 15 / 16 = 0 Rest 15 (F)
        - Hexadezimalzahl: FF

4. Binär -> Oktal:
    - Beispiel: Binärzahl 101101
        - Gruppierung: 101 101
        - Oktalzahl: 55

5. Binär -> Dezimal:
    - Beispiel: Binärzahl 1101
        - 1 * 2^0 + 0 * 2^1 + 1 * 2^2 + 1 * 2^3 = 1 + 0 + 4 + 8 = 13

6. Binär -> Hexadezimal:
    - Beispiel: Binärzahl 101101
        - Gruppierung: 10 1101
        - Hexadezimalzahl: 2D

7. Oktal -> Binär:
    - Beispiel: Oktalzahl 72
        - Oktalzahl in Binärcode: 111 010

8. Oktal -> Dezimal:
    - Beispiel: Oktalzahl 127
        - 7 * 8^0 + 2 * 8^1 + 1 * 8^2 = 7 + 16 + 64 = 87

9. Oktal -> Hexadezimal:
    - Beispiel: Oktalzahl 127
        - Oktal zu Dezimal: 87
        - Dezimal zu Hexadezimal: 57

10. Hexadezimal -> Binär:
    - Beispiel: Hexadezimalzahl 1A
        - Hexadezimal zu Binär: 0001 1010

11. Hexadezimal -> Oktal:
    - Beispiel: Hexadezimalzahl A6
        - Hexadezimal zu Dezimal: 166
        - Dezimal zu Oktal: 246

12. Hexadezimal -> Dezimal:
    - Beispiel: Hexadezimalzahl FF
        - 15 * 16^0 + 15 * 16^1 = 15 + 240 = 255



## Aufgabe

| 2er | 8er | 10er | 16er |
|---|---|---|---|
| ___10010101___ | 225 | 149 | 95 | 
| 110011 | ___63___ | 51 | 33 |
| 10011100 | 234 | ___156___ | 9c |
| 1111010 | 172 | 122 | ___f2___ |

| 2er | 8er | 10er | 16er |
|---|---|---|---|
| ___1001010110010101___ | 112625 | 76586 | 9595 |
| 110 0010 0111 | ___3047___ | 1575 | 627 |
| 1100000011100 | 14034 | ___6172___ | 181c |
| 10111100011 | 2743 | 1507 | ___5e3___ |
