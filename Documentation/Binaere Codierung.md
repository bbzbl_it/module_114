# Binäre Codierung

## Addition von Binärzahlen

|  | 128 | 64 | 32 | 16 | 8 | 4 | 2 | 1 |
|---|---|---|---|---|---|---|---|---|
|   | 1 | 0 | 0 | 1 | 1 | 0 | 0 | 1 |
| + | 0 | 0 | 1 | 1 | 0 | 1 | 0 | 1 |
|---|---|---|---|---|---|---|---|---|
|   |  | 1 | 1 |  |  |  | 1 |  |
|   | 1 | 1 | 0 | 0 | 1 | 1 | 1 | 0 |


|  | 128 | 64 | 32 | 16 | 8 | 4 | 2 | 1 |
|---|---|---|---|---|---|---|---|---|
|   | 1 | 1 | 1 | 1 | 1 | 1 | 1 | 1 |
| + | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 1 |
|---|---|---|---|---|---|---|---|---|
|  | 1 | 1 | 1 | 1 | 1 | 1 | 1 |  |
|  | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |


## Einerkomplement


### Was ist der Nachteil des Einerkomplements?

Der Nachteil des Einerkomplements ist, das die 4 Bit nun nur noch bis zu 7 hochzählen können, und nicht mehr bis 15. Zudem ist 1 "verschwendet", da es `0000 = 0` und `1111 = -0`  gibt, obwohl `0 == -0` ist.

## Negative Dezimalzahlen als Binärzahlen

### Rechnen

- -83 (8 Bit)

 83 > `Binär` > 0101 0011
 0101 0011 > `Einerkomplement` > 1010 1100
 1010 1100 > `+ 1` > 1010 1101
 
 __Lösung: 1010 1100__



- -24000 (16 Bit)
  
 24000 > `Binär` > 0101 1101 1100 0000
 0101 1101 1100 0000 > `Einerkomplement` > 1010 0010 0011 1111
 1010 0010 0011 1111 > `+ 1` > 1010 0010 0100 0000
 
 __Lösung: 1010 0010 0100 0000__


### Hack

- -83 (8 Bit)

 83 > `Binär` > 0101 0011
 0101 001|1 > `Invertieren, aber das letzte 1 (und alles danach) übernehmen` > 1010 1101
 __Lösung: 1010 1100__



- -24000 (16 Bit)
  
 24000 > `Binär` > 0101 1101 1100 0000
 0101 1101 1|100 0000 > `Invertieren, aber das letzte 1 (und alles danach) übernehmen` > 1010 0010 0100 0000
 
 __Lösung: 1010 0010 0100 0000__

