# Module_114


## Getting started

You can either look at the code in the Repo here, or you can visit https://bbzbl_it.gitlab.io/module_114 for a mkdocs site.


## More Resources

| Resource | Link |
|----------|------|
| My Docs | https://bbzbl_it.gitlab.io/module_114 |
| Modul Website | https://sites.google.com/bbzbl-it.ch/modul-114-ina22a/ |
